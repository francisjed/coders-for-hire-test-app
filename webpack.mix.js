let mix = require('laravel-mix')

// Although laravel-mix is optimized for Laravel App, it can also be used for different types of applications
mix.js('src/js/app.js', 'dist/')
  .sass('src/scss/app.scss', 'dist/')
