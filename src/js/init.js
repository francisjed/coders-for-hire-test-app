import sampleApp from './SampleApp'

/**
 * A config block to setup configuration for the application and routes
 */
sampleApp.config(function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.when('', '/')

  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'components/home.html'
    })

    .state('signup', {
      url: '/signup',
      templateUrl: 'components/signup.html'
    })

    .state('login', {
      url: '/login',
      templateUrl: 'components/login.html'
    })

    .state('profile', {
      url: '/profile',
      templateUrl: 'components/profile.html',
      resolve: {
        authorize: ['authorization',
          function (authorization) {
            authorization.authorize()
          }
        ]
      }
    })

    .state('logout', {
      url: '/logout',
      controller: ['authUser', '$state',
        function (authUser, $state) {
          authUser.logout()
          $state.go('home')
        }
      ]
    })
})

/**
 * A run block to kickstart the application
 */
sampleApp.run(['$rootScope', '$state', 'authorization', function ($rootScope, $state, authorization) {
  // Always check for authorization for every state changes
  $rootScope.$on('$stateChangeStart', function (event, to, params) {
    authorization.authorize()
  })
}])
