import sampleApp from '../SampleApp'

/**
 * Service for storing and checking authenticated user
 * @author francisjed <francisjedguipo@gmail.com>
 */
sampleApp.factory('authUser', ['$localStorage', function ($localStorage) {

  if (typeof $localStorage.authUser === 'undefined') {
    $localStorage.authUser = null
  }

  return {
    /**
     * Checks if user is authenticated
     * @returns {boolean}
     */
    isAuthenticated () {
      return $localStorage.authUser !== null
    },

    /**
     * Authenticate user for login
     * @param user
     */
    authenticate (user) {
      $localStorage.authUser = user
    },

    /**
     * Get authenticated user
     * @returns {null|*}
     */
    getUser () {
      return $localStorage.authUser
    },

    /**
     * Logout user
     */
    logout () {
      $localStorage.authUser = null
    }
  }
}])
