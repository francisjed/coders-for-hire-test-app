import sampleApp from '../SampleApp'

/**
 * Service for authorizing user access
 * @author francisjed <francisjedguipo@gmail.com>
 */
sampleApp.factory('authorization', ['authUser', '$state', function (authUser, $state) {
  return {

    /**
     * Checks user authorization
     */
    authorize () {
      if (!authUser.isAuthenticated()) {
        $state.go('login')
      }
    }
  }
}])
