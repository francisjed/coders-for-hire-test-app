import sampleApp from '../SampleApp'
import db from '../database'
import validate from 'validate.js'

const RULES = {
  firstName: {
    presence: true
  },
  lastName: {
    presence: true
  },
  password: {
    presence: true
  },
  confirmPassword: {
    equality: 'password'
  },
  email: {
    presence: true,
    email: true
  }
}

/**
 * Controller for signup form
 * @author francisjed <francisjedguipo@gmail.com>
 * @param $scope
 * @param $state
 */
const SignupController = function ($scope, $state) {
  /**
   * Handler for submitting user signup
   */
  $scope.submit = function () {
    const {email, firstName, lastName, password, confirmPassword} = $scope
    const user = {email, firstName, lastName, password}
    const errors = validate({...user, confirmPassword}, RULES, {format: 'flat'})

    $scope.errors = []
    $scope.success = false

    if (typeof errors !== 'undefined') {
      $scope.errors = [...$scope.errors, ...errors]
      return
    }

    let existingUser = db.get('users').find({email: email}).value()
    if (typeof existingUser !== 'undefined') {
      $scope.errors = ['Email address was already used.']
      return
    }

    db.get('users').push(user).write()
    $scope.success = true

    setTimeout(function () {
      $state.go('login')
    }, 2000)
  }
}

sampleApp.controller('SignupController', ['$scope', '$state', SignupController])
