import sampleApp from '../SampleApp'
import db from '../database'

/**
 * Controller for login form
 * @param $scope
 * @param $state
 * @constructor
 */
let LoginController = function ($scope, $state, authUser) {
  /**
   * Handler for user login
   */
  $scope.login = function () {
    const {email, password} = $scope
    let user = db.get('users').find({email, password}).value()

    $scope.errors = []
    if (typeof user === 'undefined') {
      $scope.errors = ['Invalid email or password.']
      return
    }

    authUser.authenticate(user)
    $state.go('profile')
  }
}

sampleApp.controller('LoginController', ['$scope', '$state', 'authUser', LoginController])
