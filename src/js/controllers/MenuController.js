import sampleApp from '../SampleApp'

/**
 * Controller for user profile
 * @param $scope
 * @param authUser
 * @constructor
 */
let MenuController = function ($scope, authUser) {
  $scope.$watch(() => authUser.getUser(), (user) => {
    $scope.isLoggedIn = authUser.isAuthenticated()
    if (user) {
      $scope.firstName = user.firstName
    }
  })
}

sampleApp.controller('MenuController', ['$scope', 'authUser', MenuController])
