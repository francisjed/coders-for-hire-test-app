import sampleApp from '../SampleApp'

/**
 * Controller for user profile
 * @param $scope
 * @param authUser
 * @constructor
 */
let ProfileController = function ($scope, authUser) {
  const user = authUser.getUser()

  $scope.firstName = user.firstName
  $scope.lastName = user.lastName
  $scope.email = user.email
}

sampleApp.controller('ProfileController', ['$scope', 'authUser', ProfileController])
