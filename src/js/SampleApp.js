import 'angular'
import 'angular-ui-router'
import 'ngstorage'

/**
 * Creates a singleton for angular app
 */
export default window.angular.module('sampleApp', ['ngStorage', 'ui.router'])
