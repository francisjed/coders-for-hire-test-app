/**
 * Lodash powered mini database
 * @author francisjed <francisjedguipo@gmail.com>
 */

import low from 'lowdb'
import LocalStorage from 'lowdb/adapters/LocalStorage'

let adapter = new LocalStorage('db')
let db = low(adapter)

db.defaults({users: []}).write()

export default db
