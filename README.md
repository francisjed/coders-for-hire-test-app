# The App
A sample single page application for Coders for Hire Recruitment Test.

### Requirements
- NodeJS (latest version)
- Yarn

### Setup
```bash
git clone git@gitlab.com:francisjed/coders-for-hire-test-app.git
cd coders-for-hire-test-app
yarn install
```

### Running the application
```bash
yarn run dev
yarn run serve
```
